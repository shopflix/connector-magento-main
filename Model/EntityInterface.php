<?php
/**
 * EntityInterface.php
 *
 * @copyright Copyright © 2021 Onecode  All rights reserved.
 * @author    Spyros Bodinis {spyros@onecode.gr}
 */

namespace Onecode\ShopFlixConnector\Model;

interface EntityInterface
{
    /**
     * @return string
     */
    public function getIncrementId();
}
